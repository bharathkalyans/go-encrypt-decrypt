build:
	go build main.go
run:
	go run ./main.go --file ./imp-data.txt --key "bHB&fuw2GD*I@B23" --out ./imp-data-enc.txt
encrypt:
	go run ./main.go encrypt --file ./imp-data.txt --key "bHB&fuw2GD*I@B23" --out ./imp-data-enc.txt
decrypt:
	go run ./main.go decrypt --file ./imp-data-enc.txt --key "bHB&fuw2GD*I@B23" --out ./imp-data-dec.txt
delete_file:	
	rm -f main imp-data-enc.txt imp-data-dec.txt 



# all:
# 	run delete_file