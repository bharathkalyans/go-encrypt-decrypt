package main

import (
	"crypto/aes"
	"crypto/cipher"
	"flag"
	"fmt"
	"os"
	"time"
)

const (
	DefaultInputFile  = "imp-data.txt"
	DefaultOutputFile = "imp-data-enc.txt"
	DefaultKey        = "bHB&fuw2GD*I@B23"
	ChunkSize         = 1024
)

func main() {

	if len(os.Args) < 8 {
		fmt.Println("Wrong number of Arguments passed")
		return
	}

	start_time := time.Now()
	filePtr := flag.String("file", DefaultInputFile, "Filename of the Input file")
	keyPtr := flag.String("key", DefaultKey, "Key used to encrypt the File")
	outPtr := flag.String("out", DefaultOutputFile, "Filename of the Output file")

	flag.Parse()
	//Flags
	// fmt.Println(*filePtr, *outPtr, *keyPtr)
	operation := os.Args[1]
	fmt.Println("Operation :: ", operation)

	key := []byte(*keyPtr)
	switch operation {
	case "encrypt":
		startEncrypting(key, *filePtr, *outPtr)
	case "decrypt":
		startDecrypting(key, *outPtr, "imp-data-dec.txt")
	default:
		fmt.Println("Wrong Arguments passed for Operation!!")
	}

	fmt.Println("Total Execution Time :: ", time.Since(start_time))
}

func chunkSlice(slice []byte) [][]byte {
	var chunks [][]byte
	for i := 0; i < len(slice); i += ChunkSize {
		end := i + ChunkSize

		if end > len(slice) {
			end = len(slice)
		}

		chunks = append(chunks, slice[i:end])
	}

	return chunks
}

func startEncrypting(key []byte, input_file string, output_file string) {

	block, err := aes.NewCipher(key)
	if err != nil {
		fmt.Println("Unable to Encrypt the Data:", err)
		return
	}
	stream := cipher.NewCTR(block, make([]byte, aes.BlockSize))

	file, err := os.Create(output_file)
	if err != nil {
		fmt.Println("Error creating output file:", err)
		return
	}
	defer file.Close()

	inputFile, err := os.ReadFile(input_file)
	if err != nil {
		fmt.Println("Error :: ", err)
		return
	}

	// myFile, _ := os.Open(input_file)
	// myFile.Stat()

	chunks := chunkSlice(inputFile)

	for _, chunk := range chunks {
		encryptFile(stream, chunk, file)
	}
}

func encryptFile(stream cipher.Stream, plaintext []byte, file *os.File) {
	ciphertext := make([]byte, len(plaintext))
	stream.XORKeyStream(ciphertext, plaintext)

	_, err := file.Write(ciphertext)
	if err != nil {
		fmt.Println("Error writing to output file:", err)
		return
	}
}

func startDecrypting(key []byte, encrypted_file string, decrypted_file string) {

	block, err := aes.NewCipher(key)
	if err != nil {
		fmt.Println("Error while creating cipher key!!!!!!!!")
		return
	}
	stream := cipher.NewCTR(block, make([]byte, aes.BlockSize))

	file, err := os.Create(decrypted_file)
	if err != nil {
		fmt.Println("Error creating output file:", err)
		return
	}
	defer file.Close()

	encryptedFile, err := os.ReadFile(encrypted_file)
	if err != nil {
		fmt.Println("Error Opening the Decrypted File! ", err)
		return
	}

	chunks := chunkSlice(encryptedFile)

	for _, chunk := range chunks {
		decryptingFile(stream, chunk, file)
	}
}

func decryptingFile(stream cipher.Stream, ciphertext []byte, file *os.File) {
	plaintext := make([]byte, len(ciphertext))
	stream.XORKeyStream(plaintext, ciphertext)

	_, err := file.Write(plaintext)
	if err != nil {
		fmt.Println("Error writing to output file:", err)
		return
	}
}
