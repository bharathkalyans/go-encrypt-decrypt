

### Run the Program
- Makefile is already added
- Use command `make build` to build the CLI app
- To encrypt the file use `make encrypt`
- To decryptm the file use `make decrypt`

File Size ~ 900 MB
BenchMark ScreenShot

![Image](./images/SS.png)


[Golang Gitub Webknot Blog](https://gowebknot.github.io/tracks/golang/2024/01/22/golang-fundamentals/)

[Assignment Docs](https://docs.google.com/document/d/1ds5KoXchIJXVu8UIeuog3KwV5kH5nKiLNNIuF5Pocic/edit)

#### Resources Used
* [Learn Go Fast: Tutorial](https://www.youtube.com/watch?v=8uiZC0l4Ajw&t=1870s)
* [Reading files in Go — an overview](https://kgrz.io/reading-files-in-go-an-overview.html#reading-file-chunks-concurrently)
